package com.security.test.repository;

import com.security.test.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

@Repository
@Transactional(propagation = REQUIRES_NEW)
public interface UserRepository extends CrudRepository<User, Long> {

    User findUserByUsername(String username);
}
