package com.security.test;

import com.security.test.dto.UserAuthenticationInfoDto;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @RestController
    public class SecurityController {

        @PostMapping("/auth")
        public ResponseEntity<String> authenticate(@RequestBody UserAuthenticationInfoDto userAuthenticationInfo) {
            return ResponseEntity.ok("");
        }
    }

    @RestController
    @RequestMapping("/info")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public class InfoController {

        @GetMapping
        public ResponseEntity<String> getInfo() {
            return ResponseEntity.ok("That's the whole information we have.");
        }
    }
}
