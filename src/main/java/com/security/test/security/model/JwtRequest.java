package com.security.test.security.model;

import java.io.Serializable;

public class JwtRequest implements Serializable {

    private String username;
    private String password;
    private String identity;

    public JwtRequest() {
        //default constructor for JSON Parsing
    }

    public JwtRequest(String username, String password, String identity) {
        this.username = username;
        this.password = password;
        this.identity = identity;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }
}
